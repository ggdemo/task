Next IT Services - Demo application
======================================================================

![Interface screenshot](https://i.imgur.com/Me0wZQa.png)

Demo SPA built with Vue 3.

**Public access**: [http://next-it.gani.bg](http://next-it.gani.bg/)


## Project setup

> Make sure that you have the latest stable node and npm installed.

1. Download/clone the repo and navigate to the project directory.
2. Install node dependencies: `npm install`.
3. Start the development server: `npm run serve`.

To generate production build for deployment, run `npm run build`.

> Built files are meant to be served over HTTP. Opening dist/index.html over file:// won't work.


## Notes

- Local storage is used for data persistence.
- The ID field is not "public editable" to emulate the default database auto increment behavior.
