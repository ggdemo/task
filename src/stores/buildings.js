/**
 * Persists the specified state to local storage.
 *
 * @param {Object} state
 */
function saveState(state) {
  localStorage.setItem('state', JSON.stringify(state));
}

/**
 * Extracts and returns the persisted buildings state from the local storage.
 *
 * @return {Object} The stored state (or a default one, if missing)
 */
function getSavedState() {
  const raw = localStorage.getItem('state') || '';

  try {
    return JSON.parse(raw);
  } catch(e) {
    return {
      buildings: [],
      lastAutoIncrement: 0,
    };
  }
}

// buildings store module
export default {
  namespaced: true,
  state() {
    return getSavedState();
  },
  mutations: {
    reset(state) {
      state.buildings         = [];
      state.lastAutoIncrement = 0;
    },
    refreshLastAutoIncrement(state) {
      // reset
      state.lastAutoIncrement = 0;

      // sync with the largest building id
      for (let building of state.buildings) {
        if (state.lastAutoIncrement < building.id) {
          state.lastAutoIncrement = building.id;
        }
      }
    },
    addOrReplace(state, building) {
      building = Object.assign({}, building); // clean shallow copy

      if (!Object.keys(building).length) {
        return; // nothing to add
      }

      // replace existing
      for (let i = state.buildings.length - 1; i >= 0; i--) {
        if (state.buildings[i].id == building.id) {
          state.buildings[i] = building;
          return;
        }
      }

      // ...or push new
      if (!building.id) {
        building.id = ++state.lastAutoIncrement;
      }
      state.buildings.push(building);
    },
    remove(state, id) {
      for (let i = state.buildings.length - 1; i >= 0; i--) {
        if (state.buildings[i].id == id) {
          state.buildings.splice(i, 1);
          break;
        }
      }
    },
  },
  actions: {
    set(context, buildings = []) {
      context.commit("reset");

      for (let building of buildings) {
        context.commit("addOrReplace", building);
      }

      context.commit("refreshLastAutoIncrement");

      saveState(context.state);
    },
    addOrReplace(context, building) {
      context.commit("addOrReplace", building);

      saveState(context.state);
    },
    remove(context, id) {
      context.commit("remove", id);

      saveState(context.state);
    },
  },
  getters: {
    getById: (state) => (id) => {
      if (id) {
        for (let i = state.buildings.length - 1; i >= 0; i--) {
          if (state.buildings[i].id == id) {
            return state.buildings[i];
          }
        }
      }

      return null;
    },
  },
};
