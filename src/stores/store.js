import { createStore } from "vuex";
import buildings       from "@/stores/buildings";

export default createStore({
  modules: {
    buildings: buildings,
  },
});
