import { createRouter, createWebHashHistory } from "vue-router";
import Home      from "./views/Home.vue";
import Buildings from "./views/Buildings.vue";

const routes = [
  {
    path:      "/",
    name:      "home",
    component: Home,
  },
  {
    path:      "/buildings",
    name:      "buildings",
    component: Buildings,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
