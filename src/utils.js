/**
 * Generates random string (eg. for dom ids and keys).
 *
 * @param  {Number} [length]
 * @return {String}
 */
export function randomString(length = 5) {
  let result = "";
  const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < length; i++) {
    result += alphabet.charAt(Math.floor(Math.random() * alphabet.length));
  }

  return result;
}

/**
 * Creates a thumbnail from `File` with the specified `width` and `height` arguments.
 * Returns a `Promise` with the generated base64 data url.
 *
 * @param  {File}   file
 * @param  {Number} [width]
 * @param  {Number} [height]
 * @return {Promise}
 */
export function generateThumb(file, width = 100, height = 100) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();

    reader.onload = function (e) {
      const img = new Image();

      img.onload = function () {
        const canvas    = document.createElement("canvas");
        const ctx       = canvas.getContext("2d");
        const imgWidth  = img.width;
        const imgHeight = img.height;

        canvas.width  = width;
        canvas.height = height;

        ctx.drawImage(
          img,
          imgWidth > imgHeight ? (imgWidth - imgHeight) / 2 : 0,
          0, // top aligned
          imgWidth > imgHeight ? imgHeight : imgWidth,
          imgWidth > imgHeight ? imgHeight : imgWidth,
          0,
          0,
          width,
          height
        );

        return resolve(canvas.toDataURL(file.type));
      };

      img.onerror = function () {
        return reject(new Error("Unable to load thumb."));
      };

      img.src = e.target.result;
    };

    reader.onerror = function () {
      return reject(new Error("Unable to read image file."));
    };

    reader.readAsDataURL(file);
  });
}
